<!doctype html>
<html class="no-js" lang="">

    <!-- Head -->
    <?php include('inc/head.inc.php') ?>
    <!-- -->

    <body>

        <div class="page page_bg">

            <!-- Header -->
            <?php include('inc/header.inc.php') ?>
            <!-- -->

            <section class="main">
                <div class="container">
                    <div class="heading">
                        <h1 class="side_padding">Projekte</h1>
                    </div>

                    <div class="main__row">

                        <div class="main__sidebar">
                            <div class="sidebar">
                                <div class="sidebar__heading">SUSHE</div>
                                <div class="sidebar__block">
                                    <div class="sidebar__title"><span>STADT</span></div>
                                    <ul>
                                        <li class="active"><a href="#"><span>Any</span></a></li>
                                        <li><a href="#"><span>Berlin</span></a></li>
                                        <li><a href="#"><span>Hamburg</span></a></li>
                                        <li><a href="#"><span>Stuttgart</span></a></li>
                                        <li><a href="#"><span>Frankfurt am Main</span></a></li>
                                        <li><a href="#"><span>Düsseldorf</span></a></li>
                                        <li><a href="#"><span>Stuttgart</span></a></li>
                                        <li><a href="#"><span>Dortmund</span></a></li>
                                        <li><a href="#"><span>Essen</span></a></li>
                                    </ul>
                                </div>
                                <div class="sidebar__block">
                                    <div class="sidebar__title"><span>KATEGORIEN</span></div>
                                    <ul>
                                        <li><a href="#"><span>Kategorie 1</span></a></li>
                                        <li><a href="#"><span>Kategorie 2</span></a></li>
                                        <li><a href="#"><span>Kategorie 3</span></a></li>
                                        <li><a href="#"><span>Kategorie 4</span></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="main__content">

                            <div class="panel">
                                <div class="panel__nav">
                                    <a class="active" href="#">Blöcke</a>
                                    <a href="#">Karte</a>
                                </div>
                                <div class="panel__settings">
                                    <div class="panel__filter">
                                        <a class="active" href="#">nach preis</a>
                                        <a href="#">nach belegdatum</a>
                                    </div>
                                    <div class="panel__view">
                                        <a class="active" href="#">9</a>
                                        <a href="#">15</a>
                                        <a href="#">30</a>
                                    </div>
                                </div>
                            </div>

                            <div class="content">
                                <div class="search">
                                    <div class="search__wrap">
                                        <input type="text" class="search__input" name="" placeholder="">
                                        <button type="submit" class="search__button"></button>
                                    </div>
                                </div>
                                <div class="content__wrap">

                                    <div class="row_wrap">
                                        <div class="row">

                                            <div class="col col-xs-12 col-sm-6 col-md-6 col-lg-4 col-xl-4">
                                                <div class="goods">
                                                    <a href="#" class="goods__image">
                                                        <span class="goods__city">Berlin</span>
                                                        <span class="goods__date">15.11.2014</span>
                                                        <img src="images/pr_01.jpg" class="img-fluid" alt="">
                                                    </a>
                                                    <div class="goods__info">
                                                        <div class="goods__name"><a href="#">Mauerstraße, 45</a></div>
                                                        <div class="goods__tags">
                                                            <a href="#">Kategorie 1</a>, <a href="#">Kategorie 2</a>, <a href="#">Kategorie 3</a>, <a href="#">Kategorie 4</a>
                                                        </div>
                                                        <div class="goods__text">Kameraden! Die Weiterentwicklung der verschiedenen Aktivitätsformen erfordert die Einführung und Modernisierung des Systems der Personalschulung und erfüllt dringende Bedürfnisse. Es darf jedoch nicht vergessen werden, dass es immer weiter geht. Es darf jedoch </div>
                                                    </div>
                                                    <div class="goods__bottom">
                                                        <div class="goods__price">von €35</div>
                                                        <a class="goods__view" href="#">
                                                            <span>Mehr</span>
                                                            <i>
                                                                <svg class="ico-svg" viewBox="0 0 12 22" xmlns="http://www.w3.org/2000/svg">
                                                                    <use xlink:href="img/sprite_icons.svg#icon__chevron_right" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                                </svg>
                                                            </i>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col col-xs-12 col-sm-6 col-md-6 col-lg-4 col-xl-4">
                                                <div class="goods">
                                                    <a href="#" class="goods__image">
                                                        <span class="goods__city">Berlin</span>
                                                        <span class="goods__date">15.11.2014</span>
                                                        <img src="images/pr_02.jpg" class="img-fluid" alt="">
                                                    </a>
                                                    <div class="goods__info">
                                                        <div class="goods__name"><a href="#">Mauerstraße, 45</a></div>
                                                        <div class="goods__tags">
                                                            <a href="#">Kategorie 1</a>, <a href="#">Kategorie 2</a>, <a href="#">Kategorie 3</a>, <a href="#">Kategorie 4</a>
                                                        </div>
                                                        <div class="goods__text">Kameraden! Die Weiterentwicklung der verschiedenen Aktivitätsformen erfordert die Einführung und Modernisierung des Systems der Personalschulung und erfüllt dringende Bedürfnisse. Es darf jedoch nicht vergessen werden, dass es immer weiter geht. Es darf jedoch </div>
                                                    </div>
                                                    <div class="goods__bottom">
                                                        <div class="goods__price">von €35</div>
                                                        <a class="goods__view" href="#">
                                                            <span>Mehr</span>
                                                            <i>
                                                                <svg class="ico-svg" viewBox="0 0 12 22" xmlns="http://www.w3.org/2000/svg">
                                                                    <use xlink:href="img/sprite_icons.svg#icon__chevron_right" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                                </svg>
                                                            </i>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col col-xs-12 col-sm-6 col-md-6 col-lg-4 col-xl-4">
                                                <div class="goods">
                                                    <a href="#" class="goods__image">
                                                        <span class="goods__city">Berlin</span>
                                                        <span class="goods__date">15.11.2014</span>
                                                        <img src="images/pr_03.jpg" class="img-fluid" alt="">
                                                    </a>
                                                    <div class="goods__info">
                                                        <div class="goods__name"><a href="#">Mauerstraße, 45</a></div>
                                                        <div class="goods__tags">
                                                            <a href="#">Kategorie 1</a>, <a href="#">Kategorie 2</a>, <a href="#">Kategorie 3</a>, <a href="#">Kategorie 4</a>
                                                        </div>
                                                        <div class="goods__text">Kameraden! Die Weiterentwicklung der verschiedenen Aktivitätsformen erfordert die Einführung und Modernisierung des Systems der Personalschulung und erfüllt dringende Bedürfnisse. Es darf jedoch nicht vergessen werden, dass es immer weiter geht. Es darf jedoch </div>
                                                    </div>
                                                    <div class="goods__bottom">
                                                        <div class="goods__price">von €35</div>
                                                        <a class="goods__view" href="#">
                                                            <span>Mehr</span>
                                                            <i>
                                                                <svg class="ico-svg" viewBox="0 0 12 22" xmlns="http://www.w3.org/2000/svg">
                                                                    <use xlink:href="img/sprite_icons.svg#icon__chevron_right" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                                </svg>
                                                            </i>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col col-xs-12 col-sm-6 col-md-6 col-lg-4 col-xl-4">
                                                <div class="goods">
                                                    <a href="#" class="goods__image">
                                                        <span class="goods__city">Berlin</span>
                                                        <span class="goods__date">15.11.2014</span>
                                                        <img src="images/pr_01.jpg" class="img-fluid" alt="">
                                                    </a>
                                                    <div class="goods__info">
                                                        <div class="goods__name"><a href="#">Mauerstraße, 45</a></div>
                                                        <div class="goods__tags">
                                                            <a href="#">Kategorie 1</a>, <a href="#">Kategorie 2</a>, <a href="#">Kategorie 3</a>, <a href="#">Kategorie 4</a>
                                                        </div>
                                                        <div class="goods__text">Kameraden! Die Weiterentwicklung der verschiedenen Aktivitätsformen erfordert die Einführung und Modernisierung des Systems der Personalschulung und erfüllt dringende Bedürfnisse. Es darf jedoch nicht vergessen werden, dass es immer weiter geht. Es darf jedoch </div>
                                                    </div>
                                                    <div class="goods__bottom">
                                                        <div class="goods__price">von €35</div>
                                                        <a class="goods__view" href="#">
                                                            <span>Mehr</span>
                                                            <i>
                                                                <svg class="ico-svg" viewBox="0 0 12 22" xmlns="http://www.w3.org/2000/svg">
                                                                    <use xlink:href="img/sprite_icons.svg#icon__chevron_right" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                                </svg>
                                                            </i>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col col-xs-12 col-sm-6 col-md-6 col-lg-4 col-xl-4">
                                                <div class="goods">
                                                    <a href="#" class="goods__image">
                                                        <span class="goods__city">Berlin</span>
                                                        <span class="goods__date">15.11.2014</span>
                                                        <img src="images/pr_02.jpg" class="img-fluid" alt="">
                                                    </a>
                                                    <div class="goods__info">
                                                        <div class="goods__name"><a href="#">Mauerstraße, 45</a></div>
                                                        <div class="goods__tags">
                                                            <a href="#">Kategorie 1</a>, <a href="#">Kategorie 2</a>, <a href="#">Kategorie 3</a>, <a href="#">Kategorie 4</a>
                                                        </div>
                                                        <div class="goods__text">Kameraden! Die Weiterentwicklung der verschiedenen Aktivitätsformen erfordert die Einführung und Modernisierung des Systems der Personalschulung und erfüllt dringende Bedürfnisse. Es darf jedoch nicht vergessen werden, dass es immer weiter geht. Es darf jedoch </div>
                                                    </div>
                                                    <div class="goods__bottom">
                                                        <div class="goods__price">von €35</div>
                                                        <a class="goods__view" href="#">
                                                            <span>Mehr</span>
                                                            <i>
                                                                <svg class="ico-svg" viewBox="0 0 12 22" xmlns="http://www.w3.org/2000/svg">
                                                                    <use xlink:href="img/sprite_icons.svg#icon__chevron_right" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                                </svg>
                                                            </i>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col col-xs-12 col-sm-6 col-md-6 col-lg-4 col-xl-4">
                                                <div class="goods">
                                                    <a href="#" class="goods__image">
                                                        <span class="goods__city">Berlin</span>
                                                        <span class="goods__date">15.11.2014</span>
                                                        <img src="images/pr_03.jpg" class="img-fluid" alt="">
                                                    </a>
                                                    <div class="goods__info">
                                                        <div class="goods__name"><a href="#">Mauerstraße, 45</a></div>
                                                        <div class="goods__tags">
                                                            <a href="#">Kategorie 1</a>, <a href="#">Kategorie 2</a>, <a href="#">Kategorie 3</a>, <a href="#">Kategorie 4</a>
                                                        </div>
                                                        <div class="goods__text">Kameraden! Die Weiterentwicklung der verschiedenen Aktivitätsformen erfordert die Einführung und Modernisierung des Systems der Personalschulung und erfüllt dringende Bedürfnisse. Es darf jedoch nicht vergessen werden, dass es immer weiter geht. Es darf jedoch </div>
                                                    </div>
                                                    <div class="goods__bottom">
                                                        <div class="goods__price">von €35</div>
                                                        <a class="goods__view" href="#">
                                                            <span>Mehr</span>
                                                            <i>
                                                                <svg class="ico-svg" viewBox="0 0 12 22" xmlns="http://www.w3.org/2000/svg">
                                                                    <use xlink:href="img/sprite_icons.svg#icon__chevron_right" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                                </svg>
                                                            </i>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col col-xs-12 col-sm-6 col-md-6 col-lg-4 col-xl-4">
                                                <div class="goods">
                                                    <a href="#" class="goods__image">
                                                        <span class="goods__city">Berlin</span>
                                                        <span class="goods__date">15.11.2014</span>
                                                        <img src="images/pr_01.jpg" class="img-fluid" alt="">
                                                    </a>
                                                    <div class="goods__info">
                                                        <div class="goods__name"><a href="#">Mauerstraße, 45</a></div>
                                                        <div class="goods__tags">
                                                            <a href="#">Kategorie 1</a>, <a href="#">Kategorie 2</a>, <a href="#">Kategorie 3</a>, <a href="#">Kategorie 4</a>
                                                        </div>
                                                        <div class="goods__text">Kameraden! Die Weiterentwicklung der verschiedenen Aktivitätsformen erfordert die Einführung und Modernisierung des Systems der Personalschulung und erfüllt dringende Bedürfnisse. Es darf jedoch nicht vergessen werden, dass es immer weiter geht. Es darf jedoch </div>
                                                    </div>
                                                    <div class="goods__bottom">
                                                        <div class="goods__price">von €35</div>
                                                        <a class="goods__view" href="#">
                                                            <span>Mehr</span>
                                                            <i>
                                                                <svg class="ico-svg" viewBox="0 0 12 22" xmlns="http://www.w3.org/2000/svg">
                                                                    <use xlink:href="img/sprite_icons.svg#icon__chevron_right" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                                </svg>
                                                            </i>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col col-xs-12 col-sm-6 col-md-6 col-lg-4 col-xl-4">
                                                <div class="goods">
                                                    <a href="#" class="goods__image">
                                                        <span class="goods__city">Berlin</span>
                                                        <span class="goods__date">15.11.2014</span>
                                                        <img src="images/pr_02.jpg" class="img-fluid" alt="">
                                                    </a>
                                                    <div class="goods__info">
                                                        <div class="goods__name"><a href="#">Mauerstraße, 45</a></div>
                                                        <div class="goods__tags">
                                                            <a href="#">Kategorie 1</a>, <a href="#">Kategorie 2</a>, <a href="#">Kategorie 3</a>, <a href="#">Kategorie 4</a>
                                                        </div>
                                                        <div class="goods__text">Kameraden! Die Weiterentwicklung der verschiedenen Aktivitätsformen erfordert die Einführung und Modernisierung des Systems der Personalschulung und erfüllt dringende Bedürfnisse. Es darf jedoch nicht vergessen werden, dass es immer weiter geht. Es darf jedoch </div>
                                                    </div>
                                                    <div class="goods__bottom">
                                                        <div class="goods__price">von €35</div>
                                                        <a class="goods__view" href="#">
                                                            <span>Mehr</span>
                                                            <i>
                                                                <svg class="ico-svg" viewBox="0 0 12 22" xmlns="http://www.w3.org/2000/svg">
                                                                    <use xlink:href="img/sprite_icons.svg#icon__chevron_right" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                                </svg>
                                                            </i>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col col-xs-12 col-sm-6 col-md-6 col-lg-4 col-xl-4">
                                                <div class="goods">
                                                    <a href="#" class="goods__image">
                                                        <span class="goods__city">Berlin</span>
                                                        <span class="goods__date">15.11.2014</span>
                                                        <img src="images/pr_03.jpg" class="img-fluid" alt="">
                                                    </a>
                                                    <div class="goods__info">
                                                        <div class="goods__name"><a href="#">Mauerstraße, 45</a></div>
                                                        <div class="goods__tags">
                                                            <a href="#">Kategorie 1</a>, <a href="#">Kategorie 2</a>, <a href="#">Kategorie 3</a>, <a href="#">Kategorie 4</a>
                                                        </div>
                                                        <div class="goods__text">Kameraden! Die Weiterentwicklung der verschiedenen Aktivitätsformen erfordert die Einführung und Modernisierung des Systems der Personalschulung und erfüllt dringende Bedürfnisse. Es darf jedoch nicht vergessen werden, dass es immer weiter geht. Es darf jedoch </div>
                                                    </div>
                                                    <div class="goods__bottom">
                                                        <div class="goods__price">von €35</div>
                                                        <a class="goods__view" href="#">
                                                            <span>Mehr</span>
                                                            <i>
                                                                <svg class="ico-svg" viewBox="0 0 12 22" xmlns="http://www.w3.org/2000/svg">
                                                                    <use xlink:href="img/sprite_icons.svg#icon__chevron_right" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                                </svg>
                                                            </i>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>


                                        </div>
                                    </div>

                                    <div class="pagination">
                                        <a class="pagination__prev" href="#"></a>

                                        <div class="pagination__page">
                                            <a href="#">1</a>
                                            <a href="#">2</a>
                                            <a href="#">3</a>
                                            <span>...</span>
                                            <a href="#">15</a>
                                            <a href="#">16</a>
                                        </div>

                                        <a class="pagination__next" href="#"></a>
                                    </div>

                                </div>
                            </div>

                        </div>

                    </div>

                </div>
            </section>

            <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
            <!-- -->

        </div>


        <!-- Modal -->
        <?php include('inc/modal.inc.php') ?>
        <!-- -->

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->


    </body>
</html>
