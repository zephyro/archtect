<!doctype html>
<html class="no-js" lang="">

    <!-- Head -->
    <?php include('inc/head.inc.php') ?>
    <!-- -->

    <body>

        <div class="page page_bg">

            <!-- Header -->
            <?php include('inc/header.inc.php') ?>
            <!-- -->

            <section class="main">
                <div class="container">

                    <div class="heading">
                        <h1>Mein Büro | <span class="color_purple text_underline">€350</span></h1>
                    </div>

                    <div class="tags">
                        <a href="#" class="active">Meine Projekte (54)</a>
                        <a href="#">Projekt hinzufügen</a>
                        <a href="#">Meine Einkäufe (32)</a>
                        <a href="#">Meine Einstellungen</a>
                    </div>

                    <div class="content">
                        <div class="content__wrap">

                            <div class="doc">
                                <div class="doc__text">
                                    <h3>BERLIN, Mauerstraße, 45</h3>
                                    <div class="doc__subtitle"><strong>Dokumentdatum:</strong> 11/03/2018</div>
                                    <p>Kameraden! Die Weiterentwicklung der verschiedenen Aktivitätsformen erfordert die Einführung und Modernisierung des Systems der Personalschulung und erfüllt dringende Bedürfnisse...</p>
                                    <p><strong>Belege, die gebucht werden:</strong></p>
                                    <ul>
                                        <li><a href="#">€45 - Dokument 1 - Titel</a> | 15 продаж | €435</li>
                                        <li><a href="#">€45 - Dokument 2 - Titel</a> | 15 продаж | €435</li>
                                        <li><a href="#">€145 - Dokument 3 - Titel</a> | 15 продаж | €435</li>
                                    </ul>
                                </div>
                                <div class="doc__action">
                                    <div class="doc__action_wrap">
                                        <button class="btn_round">Bearbeitung</button>
                                    </div>
                                </div>
                            </div>

                            <div class="doc">
                                <div class="doc__text">
                                    <h3>BERLIN, Mauerstraße, 45</h3>
                                    <div class="doc__subtitle"><strong>Dokumentdatum:</strong> 11/03/2018</div>
                                    <p>Kameraden! Die Weiterentwicklung der verschiedenen Aktivitätsformen erfordert die Einführung und Modernisierung des Systems der Personalschulung und erfüllt dringende Bedürfnisse...</p>
                                    <p><strong>Belege, die gebucht werden:</strong></p>
                                    <ul>
                                        <li><a href="#">€45 - Dokument 1 - Titel</a> | 15 продаж | €435</li>
                                        <li><a href="#">€45 - Dokument 2 - Titel</a> | 15 продаж | €435</li>
                                        <li><a href="#">€145 - Dokument 3 - Titel</a> | 15 продаж | €435</li>
                                    </ul>
                                </div>
                                <div class="doc__action">
                                    <div class="doc__action_wrap">
                                        <button class="btn_round">Bearbeitung</button>
                                    </div>
                                </div>
                            </div>

                            <div class="doc">
                                <div class="doc__text">
                                    <h3>BERLIN, Mauerstraße, 45</h3>
                                    <div class="doc__subtitle"><strong>Dokumentdatum:</strong> 11/03/2018</div>
                                    <p>Kameraden! Die Weiterentwicklung der verschiedenen Aktivitätsformen erfordert die Einführung und Modernisierung des Systems der Personalschulung und erfüllt dringende Bedürfnisse...</p>
                                    <p><strong>Belege, die gebucht werden:</strong></p>
                                    <ul>
                                        <li><a href="#">€45 - Dokument 1 - Titel</a> | 15 продаж | €435</li>
                                        <li><a href="#">€45 - Dokument 2 - Titel</a> | 15 продаж | €435</li>
                                        <li><a href="#">€145 - Dokument 3 - Titel</a> | 15 продаж | €435</li>
                                    </ul>
                                </div>
                                <div class="doc__action">
                                    <div class="doc__action_wrap">
                                        <button class="btn_round">Bearbeitung</button>
                                    </div>
                                </div>
                            </div>

                            <div class="doc mb_0">
                                <div class="doc__text">
                                    <h3>BERLIN, Mauerstraße, 45</h3>
                                    <div class="doc__subtitle"><strong>Dokumentdatum:</strong> 11/03/2018</div>
                                    <p>Kameraden! Die Weiterentwicklung der verschiedenen Aktivitätsformen erfordert die Einführung und Modernisierung des Systems der Personalschulung und erfüllt dringende Bedürfnisse...</p>
                                    <p><strong>Belege, die gebucht werden:</strong></p>
                                    <ul>
                                        <li><a href="#">€45 - Dokument 1 - Titel</a> | 15 продаж | €435</li>
                                        <li><a href="#">€45 - Dokument 2 - Titel</a> | 15 продаж | €435</li>
                                        <li><a href="#">€145 - Dokument 3 - Titel</a> | 15 продаж | €435</li>
                                    </ul>
                                </div>
                                <div class="doc__action">
                                    <div class="doc__action_wrap">
                                        <button class="btn_round">Bearbeitung</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </section>

            <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
            <!-- -->

        </div>


        <!-- Modal -->
        <?php include('inc/modal.inc.php') ?>
        <!-- -->

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->


    </body>
</html>
