<footer class="footer">
    <div class="footer__image">
        <img src="img/footer__image.png" class="img-fluid" alt="">
    </div>
    <div class="footer__bottom">
        <div class="container">
            <a class="footer__logo" href="#">
                <img src="img/footer__logo.svg" alt="" class="img-fluid">
            </a>
        </div>
    </div>
</footer>