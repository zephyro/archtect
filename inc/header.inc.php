<header class="header">
    <div class="container">

        <a class="header__logo" href="#">
            <img src="img/logo.svg" class="img-fluid" alt="">
        </a>

        <a href="#" class="header__toggle nav_toggle">
            <span></span>
            <span></span>
            <span></span>
        </a>

        <a href="#" class="header__cart">
            <span>5</span>
        </a>

        <nav class="header__nav">
            <ul>
                <li><a href="#"><span>HOME</span></a></li>
                <li><a href="#"><span>FAQ</span></a></li>
                <li><a href="#"><span>Anmeldung</span></a></li>
                <li>
                    <a href="#"><span>Mein Büro</span></a>
                    <div class="nav_dropdown">
                        <ul>
                            <li><a href="#"><span>PROJEKT HINZUFÜGEN</span></a></li>
                            <li><a href="#"><span>Meine Projekte (54)</span></a></li>
                            <li><a href="#"><span>Meine Einkäufe (32)</span></a></li>
                            <li><a href="#"><span>meine Einstellungen</span></a></li>
                        </ul>
                    </div>
                </li>
            </ul>
        </nav>

    </div>
</header>