<!doctype html>
<html class="no-js" lang="">

    <!-- Head -->
    <?php include('inc/head.inc.php') ?>
    <!-- -->

    <body>

        <div class="page page_bg">

            <!-- Header -->
            <?php include('inc/header.inc.php') ?>
            <!-- -->

            <section class="main">
                <div class="container">

                    <div class="heading">
                        <h1>Mein Büro | <span class="color_purple text_underline">€350</span></h1>
                    </div>

                    <div class="tags">
                        <a href="#">Meine Projekte (54)</a>
                        <a href="#">Projekt hinzufügen</a>
                        <a href="#" class="active">Meine Einkäufe (32)</a>
                        <a href="#">Meine Einstellungen</a>
                    </div>

                    <div class="content">
                        <div class="content__wrap">

                            <h3 class="color_blue">BERLIN, Mauerstraße, 45</h3>

                            <div class="doc">
                                <div class="doc__image">
                                    <div class="doc__icon">
                                        <img src="img/icon__word.svg" class="img-fluid" alt="">
                                    </div>
                                </div>
                                <div class="doc__text">
                                    <h3>Arhitect-project, bERLIN Mauerstraße, 45</h3>
                                    <div class="doc__subtitle"><strong>Dokumentdatum:</strong> 11/03/2018</div>
                                    <p>
                                        Kameraden! Die Weiterentwicklung der verschiedenen Aktivitätsformen erfordert die Einführung und Modernisierung des Systems der Personalschulung und erfüllt dringende Bedürfnisse.
                                    </p>
                                    <p><strong>€45, 11.12.2019, 15:23</strong></p>
                                </div>
                                <div class="doc__action">
                                    <div class="doc__action_wrap">
                                        <button class="btn_round ">Herunterladen</button>
                                    </div>
                                </div>
                            </div>

                            <div class="doc">
                                <div class="doc__image">
                                    <div class="doc__icon">
                                        <img src="img/icon__word.svg" class="img-fluid" alt="">
                                    </div>
                                </div>
                                <div class="doc__text">
                                    <h3>Arhitect-project, bERLIN Mauerstraße, 45</h3>
                                    <div class="doc__subtitle"><strong>Dokumentdatum:</strong> 11/03/2018</div>
                                    <p>
                                        Kameraden! Die Weiterentwicklung der verschiedenen Aktivitätsformen erfordert die Einführung und Modernisierung des Systems der Personalschulung und erfüllt dringende Bedürfnisse.
                                    </p>
                                    <p><strong>€45, 11.12.2019, 15:23</strong></p>
                                </div>
                                <div class="doc__action">
                                    <div class="doc__action_wrap">
                                        <button class="btn_round ">Herunterladen</button>
                                    </div>
                                </div>
                            </div>

                            <div class="doc mb_40">
                                <div class="doc__image">
                                    <div class="doc__icon">
                                        <img src="img/icon__word.svg" class="img-fluid" alt="">
                                    </div>
                                </div>
                                <div class="doc__text">
                                    <h3>Arhitect-project, bERLIN Mauerstraße, 45</h3>
                                    <div class="doc__subtitle"><strong>Dokumentdatum:</strong> 11/03/2018</div>
                                    <p>
                                        Kameraden! Die Weiterentwicklung der verschiedenen Aktivitätsformen erfordert die Einführung und Modernisierung des Systems der Personalschulung und erfüllt dringende Bedürfnisse.
                                    </p>
                                    <p><strong>€45, 11.12.2019, 15:23</strong></p>
                                </div>
                                <div class="doc__action">
                                    <div class="doc__action_wrap">
                                        <button class="btn_round ">Herunterladen</button>
                                    </div>
                                </div>
                            </div>

                            <h3 class="color_blue">BERLIN, Mauerstraße, 45</h3>

                            <div class="doc mb_40">
                                <div class="doc__image">
                                    <div class="doc__icon">
                                        <img src="img/icon__word.svg" class="img-fluid" alt="">
                                    </div>
                                </div>
                                <div class="doc__text">
                                    <h3>Arhitect-project, bERLIN Mauerstraße, 45</h3>
                                    <div class="doc__subtitle"><strong>Dokumentdatum:</strong> 11/03/2018</div>
                                    <p>
                                        Kameraden! Die Weiterentwicklung der verschiedenen Aktivitätsformen erfordert die Einführung und Modernisierung des Systems der Personalschulung und erfüllt dringende Bedürfnisse.
                                    </p>
                                    <p><strong>€45, 11.12.2019, 15:23</strong></p>
                                </div>
                                <div class="doc__action">
                                    <div class="doc__action_wrap">
                                        <button class="btn_round ">Herunterladen</button>
                                    </div>
                                </div>
                            </div>

                            <h3 class="color_blue">BERLIN, Mauerstraße, 45</h3>

                            <div class="doc">
                                <div class="doc__image">
                                    <div class="doc__icon">
                                        <img src="img/icon__word.svg" class="img-fluid" alt="">
                                    </div>
                                </div>
                                <div class="doc__text">
                                    <h3>Arhitect-project, bERLIN Mauerstraße, 45</h3>
                                    <div class="doc__subtitle"><strong>Dokumentdatum:</strong> 11/03/2018</div>
                                    <p>
                                        Kameraden! Die Weiterentwicklung der verschiedenen Aktivitätsformen erfordert die Einführung und Modernisierung des Systems der Personalschulung und erfüllt dringende Bedürfnisse.
                                    </p>
                                    <p><strong>€45, 11.12.2019, 15:23</strong></p>
                                </div>
                                <div class="doc__action">
                                    <div class="doc__action_wrap">
                                        <button class="btn_round ">Herunterladen</button>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>
            </section>

            <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
            <!-- -->

        </div>


        <!-- Modal -->
        <?php include('inc/modal.inc.php') ?>
        <!-- -->

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->


    </body>
</html>
