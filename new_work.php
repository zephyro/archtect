<!doctype html>
<html class="no-js" lang="">

    <!-- Head -->
    <?php include('inc/head.inc.php') ?>
    <!-- -->

    <body>

        <div class="page page_bg">

            <!-- Header -->
            <?php include('inc/header.inc.php') ?>
            <!-- -->

            <section class="main">
                <div class="container">

                    <div class="heading">
                        <h1>Page</h1>
                    </div>

                    <div class="content">
                        <div class="content__wrap">
                            <form class="form">

                                <div class="form_inline">
                                    <div class="form_inline__label"></div>
                                    <div class="form_inline__input">
                                        <h3>ALLGEMEINE INFORMATIONEN</h3>
                                    </div>
                                </div>

                                <div class="form_group form_inline">
                                    <div class="form_inline__label">
                                        <label class="form_label">Stadt<sup>*</sup></label>
                                    </div>
                                    <div class="form_inline__input">
                                        <input class="form_control" type="text" name="" placeholder="">
                                    </div>
                                </div>

                                <div class="form_group form_inline">
                                    <div class="form_inline__label">
                                        <label class="form_label">Adresse des Gebäudes<sup>*</sup></label>
                                    </div>
                                    <div class="form_inline__input">
                                        <input class="form_control" type="text" name="" placeholder="">
                                    </div>
                                </div>

                                <div class="form_group form_inline">
                                    <div class="form_inline__label">
                                        <label class="form_label">Grundlegende Fotografie<sup>*</sup></label>
                                    </div>
                                    <div class="form_inline__input">
                                        <label class="form_image">
                                            <input type="file" name="photo">
                                            <span class="form_image__text">Foto</span>
                                            <span class="btn btn_sm">Herunterladen</span>
                                        </label>
                                    </div>
                                </div>

                                <div class="form_group form_inline mb_50">
                                    <div class="form_inline__label">
                                        <label class="form_label">Projektbeschreibung<sup>*</sup></label>
                                    </div>
                                    <div class="form_inline__input">
                                        <textarea class="form_control" name="" placeholder="" rows="4"></textarea>
                                    </div>
                                </div>

                                <div class="form_inline">
                                    <div class="form_inline__label"></div>
                                    <div class="form_inline__input">
                                        <h3>DATEIEN HINZUFÜGEN</h3>
                                    </div>
                                </div>

                                <div class="blue_box">

                                    <div class="form_group form_inline">
                                        <div class="form_inline__label">
                                            <label class="form_label">Dateiname<sup>*</sup></label>
                                        </div>
                                        <div class="form_inline__input">
                                            <input class="form_control" type="text" name="" placeholder="">
                                        </div>
                                    </div>

                                    <div class="form_group form_inline">
                                        <div class="form_inline__label">
                                            <label class="form_label">Belegdatum<sup>*</sup></label>
                                        </div>
                                        <div class="form_inline__input">
                                            <input class="form_control" type="text" name="" placeholder="">
                                        </div>
                                    </div>

                                    <div class="form_group form_inline">
                                        <div class="form_inline__label">
                                            <label class="form_label">Preis<sup>*</sup></label>
                                        </div>
                                        <div class="form_inline__input">
                                            <div class="form_price">
                                                <input class="form_control" type="text" name="" placeholder="">
                                                <span class="form_price__currency">€</span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form_group form_inline">
                                        <div class="form_inline__label">
                                            <label class="form_label">Datei hochladen<sup>*</sup></label>
                                        </div>
                                        <div class="form_inline__input">
                                            <label class="form_file">
                                                <input type="file" class="form_file__input" value="" name="">
                                                <span class="form_file__text"></span>
                                                <span class="btn btn_sm">Herunterladen</span>
                                            </label>
                                        </div>
                                    </div>

                                    <div class="form_inline">
                                        <div class="form_inline__label">
                                            <label class="form_label">Beschreibung der Datei</label>
                                        </div>
                                        <div class="form_inline__input">
                                            <textarea class="form_control" name="" placeholder="" rows="4"></textarea>
                                        </div>
                                    </div>
                                </div>

                                <div class="blue_box">

                                    <div class="form_group form_inline">
                                        <div class="form_inline__label">
                                            <label class="form_label">Dateiname<sup>*</sup></label>
                                        </div>
                                        <div class="form_inline__input">
                                            <input class="form_control" type="text" name="" placeholder="">
                                        </div>
                                    </div>

                                    <div class="form_group form_inline">
                                        <div class="form_inline__label">
                                            <label class="form_label">Belegdatum<sup>*</sup></label>
                                        </div>
                                        <div class="form_inline__input">
                                            <input class="form_control" type="text" name="" placeholder="">
                                        </div>
                                    </div>

                                    <div class="form_group form_inline">
                                        <div class="form_inline__label">
                                            <label class="form_label">Preis<sup>*</sup></label>
                                        </div>
                                        <div class="form_inline__input">
                                            <div class="form_price">
                                                <input class="form_control" type="text" name="" placeholder="">
                                                <span class="form_price__currency">€</span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form_group form_inline">
                                        <div class="form_inline__label">
                                            <label class="form_label">Datei hochladen<sup>*</sup></label>
                                        </div>
                                        <div class="form_inline__input">
                                            <label class="form_file">
                                                <input type="file" class="form_file__input" value="https://dm.hix.one/files/42698234-nazvanie.pdf" name="">
                                                <span class="form_file__text">https://dm.hix.one/files/42698234-nazvanie.pdf</span>
                                                <span class="btn btn_sm">Herunterladen</span>
                                            </label>
                                        </div>
                                    </div>

                                    <div class=" form_inline">
                                        <div class="form_inline__label">
                                            <label class="form_label">Beschreibung der Datei</label>
                                        </div>
                                        <div class="form_inline__input">
                                            <textarea class="form_control" name="" placeholder="" rows="4">Kameraden! Die Weiterentwicklung der verschiedenen Aktivitätsformen erfordert die Einführung und Modernisierung des Systems der Personalschulung und erfüllt dringende Bedürfnisse. Es darf jedoch nicht vergessen werden, dass es immer weiter geht. Es darf jedoch </textarea>
                                        </div>
                                    </div>
                                </div>

                                <div class="blue_box text-right">
                                    <button type="button" class="btn  btn_border">Datei hinzufügen</button>
                                </div>

                                <div class="form_group form_inline">
                                    <div class="form_inline__label">

                                    </div>
                                    <div class="form_inline__input">
                                        <button type="submit" class="btn btn_md">SPEICHERN UND AUF PORTAL platzieren</button>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>

                </div>
            </section>

            <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
            <!-- -->

        </div>


        <!-- Modal -->
        <?php include('inc/modal.inc.php') ?>
        <!-- -->

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->


    </body>
</html>
