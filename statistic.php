<!doctype html>
<html class="no-js" lang="">

    <!-- Head -->
    <?php include('inc/head.inc.php') ?>
    <!-- -->

    <body>

        <div class="page page_bg">

            <!-- Header -->
            <?php include('inc/header.inc.php') ?>
            <!-- -->

            <section class="main">
                <div class="container">

                    <div class="heading">
                        <h1>Mein Büro | <span class="text_underline">€350</span></h1>
                    </div>

                    <div class="tags">
                        <a href="#">Meine Projekte (54)</a>
                        <a href="#">Projekt hinzufügen</a>
                        <a href="#">Meine Einkäufe (32)</a>
                        <a href="#">Meine Einstellungen</a>
                    </div>

                    <div class="content">
                        <div class="content__wrap">

                            <div class="blue_box">
                                <div class="box__text">
                                    <h3>+ €235 | <span class="color_blue">BERLIN, Mauerstraße, 45</span></h3>
                                    <div class="doc__subtitle"><strong>Verkaufsdatum:</strong> 11/03/2018</div>
                                    <p>Kameraden! Die Weiterentwicklung der verschiedenen Aktivitätsformen erfordert die Einführung und Modernisierung des Systems der Personalschulung und erfüllt dringende Bedürfnisse...</p>
                                    <p><strong>Verkaufte Dokumente:</strong></p>
                                    <ul>
                                        <li><a href="#">€45 - Dokument 1 - Tite</a></li>
                                        <li><a href="#">€45 - Dokument 2 - Titel</a></li>
                                        <li><a href="#">€145 - Dokument 3 - Titel</a></li>
                                    </ul>

                                </div>
                            </div>

                            <div class="rose_box">
                                <div class="box__text">
                                    <h3>- €650 | Überweisen Sie Geld auf Ihr Konto</h3>
                                    <div class="doc__subtitle"><strong>Verkaufsdatum:</strong> 11/03/2018</div>
                                    <p>Kameraden! Die Weiterentwicklung der verschiedenen Aktivitätsformen erfordert die Einführung und Modernisierung des Systems der Personalschulung und erfüllt dringende Bedürfnisse...</p>
                                </div>
                            </div>

                            <div class="blue_box">
                                <div class="box__text">
                                    <h3>+ €235 | <span class="color_blue">BERLIN, Mauerstraße, 45</span></h3>
                                    <div class="doc__subtitle"><strong>Verkaufsdatum:</strong> 11/03/2018</div>
                                    <p>Kameraden! Die Weiterentwicklung der verschiedenen Aktivitätsformen erfordert die Einführung und Modernisierung des Systems der Personalschulung und erfüllt dringende Bedürfnisse...</p>
                                    <p><strong>Verkaufte Dokumente:</strong></p>
                                    <ul>
                                        <li><a href="#">€45 - Dokument 1 - Tite</a></li>
                                        <li><a href="#">€45 - Dokument 2 - Titel</a></li>
                                        <li><a href="#">€145 - Dokument 3 - Titel</a></li>
                                    </ul>

                                </div>
                            </div>

                            <div class="blue_box mb_0">
                                <div class="box__text">
                                    <h3>+ €235 | <span class="color_blue">BERLIN, Mauerstraße, 45</span></h3>
                                    <div class="doc__subtitle"><strong>Verkaufsdatum:</strong> 11/03/2018</div>
                                    <p>Kameraden! Die Weiterentwicklung der verschiedenen Aktivitätsformen erfordert die Einführung und Modernisierung des Systems der Personalschulung und erfüllt dringende Bedürfnisse...</p>
                                    <p><strong>Verkaufte Dokumente:</strong></p>
                                    <ul>
                                        <li><a href="#">€45 - Dokument 1 - Tite</a></li>
                                        <li><a href="#">€45 - Dokument 2 - Titel</a></li>
                                        <li><a href="#">€145 - Dokument 3 - Titel</a></li>
                                    </ul>

                                </div>
                            </div>

                        </div>
                    </div>

                </div>
            </section>

            <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
            <!-- -->

        </div>


        <!-- Modal -->
        <?php include('inc/modal.inc.php') ?>
        <!-- -->

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->


    </body>
</html>
