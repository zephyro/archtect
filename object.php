<!doctype html>
<html class="no-js" lang="">

    <!-- Head -->
    <?php include('inc/head.inc.php') ?>
    <!-- -->

    <body>

        <div class="page page_bg">

            <!-- Header -->
            <?php include('inc/header.inc.php') ?>
            <!-- -->

            <section class="main">
                <div class="container">

                    <div class="heading">
                        <h1><a href="#">alle</a> | <a href="#">BERLIN</a> | Mauerstraße, 45</h1>
                    </div>

                    <div class="content">
                        <div class="content__wrap">

                            <div class="project">

                                <div class="project__image">
                                    <a href="images/project_img.jpg" class="btn_modal">
                                        <img src="images/project_img.jpg" class="img-fluid">
                                    </a>
                                </div>

                                <div class="project__info">
                                    <p>Adresse: <a href="#">Berlin</a>, Mauerstraße, 45</p>
                                    <p>Hinzugefügt: 11.15.2019, 11:32</p>
                                    <p>Datum der Dokumentation: vom 11.12.2019</p>
                                    <p>Kategorien: <a href="#">Kategorie 1</a>, <a href="#">Kategorie 2</a>, <a href="#">Kategorie 3</a></p>
                                    <p>Veröffentlichte Dokumente:</p>
                                    <p><a href="#">€45 - Dokument 1 - Titel</a></p>
                                    <p><a href="#">€45 - Dokument 2 - Titel</a></p>
                                    <p><a href="#">€145 - Dokument 3 - Titel</a></p>
                                    <p><a href="#">€245 - Dokument 4 - Titel</a></p>
                                    <p><a href="#">€245 - Dokument 5 - Titel</a></p>
                                </div>

                            </div>

                            <div class="project__content">
                                <h3>PROJEKTBESCHREIBUNG</h3>
                                <p> Kameraden! Die Weiterentwicklung der verschiedenen Aktivitätsformen erfordert die Einführung und Modernisierung des Systems der Personalschulung und erfüllt dringende Bedürfnisse. Es darf jedoch nicht vergessen werden, dass es immer weiter geht. Es darf jedoch Kameraden! Die Weiterentwicklung der verschiedenen Aktivitätsformen erfordert die Einführung und Modernisierung des Systems der Personalschulung und erfüllt dringende Bedürfnisse. Es darf jedoch nicht vergessen werden, dass es immer weiter geht. Es darf jedoch Kameraden!</p>
                                <p>Die Weiterentwicklung der verschiedenen Aktivitätsformen erfordert die Einführung und Modernisierung des Systems der Personalschulung und erfüllt dringende Bedürfnisse. Es darf jedoch nicht vergessen werden, dass es immer weiter geht.</p>
                                <p>Es darf jedoch Kameraden! Die Weiterentwicklung der verschiedenen Aktivitätsformen erfordert die Einführung und Modernisierung des Systems der Personalschulung und erfüllt dringende Bedürfnisse. Es darf jedoch nicht vergessen werden, dass es immer weiter geht. Es darf jedoch.</p>
                            </div>

                            <h3>BEIGEFÜGTE DOKUMENTE</h3>

                            <div class="doc">
                                <div class="doc__image">
                                    <div class="doc__icon">
                                        <img src="img/icon__pdf.svg" class="img-fluid" alt="">
                                    </div>
                                </div>
                                <div class="doc__text">
                                    <h3>Arhitect-project, bERLIN Mauerstraße, 45</h3>
                                    <div class="doc__subtitle"><strong>Dokumentdatum:</strong> 11/03/2018</div>
                                    <p>
                                        Kameraden! Die Weiterentwicklung der verschiedenen Aktivitätsformen erfordert die Einführung und Modernisierung des Systems der Personalschulung und erfüllt dringende Bedürfnisse.<br/>
                                        Es darf jedoch nicht vergessen werden, dass es immer weiter geht. Es darf jedoch Kameraden! Die Weiterentwicklung der verschiedenen Aktivitätsformen erfordert die Einführung und Modernisierung des Systems der Personalschulung und erfüllt dringende Bedürfnisse. Es darf jedoch nicht vergessen werden, dass es immer weiter geht. Es darf jedoch Kameraden!<br/>
                                        Die Weiterentwicklung der verschiedenen Aktivitätsformen erfordert die Einführung und Modernisierung des Systems der Personalschulung und erfüllt dringende Bedürfnisse. Es darf jedoch nicht vergessen werden, dass es immer weiter geht.<br/>
                                        Es darf jedoch Kameraden! Die Weiterentwicklung der verschiedenen Aktivitätsformen erfordert die Einführung und Modernisierung des Systems der Personalschulung und erfüllt dringende Bedürfnisse.<br/>
                                        Es darf jedoch nicht vergessen werden, dass es immer weiter geht. Es darf jedoch
                                    </p>
                                </div>
                                <div class="doc__price"><span>€45</span></div>
                                <div class="doc__action">
                                    <div class="doc__action_wrap">
                                        <button class="btn_round btn_buy">
                                            <i>
                                                <svg class="ico-svg" viewBox="0 0 33 29" xmlns="http://www.w3.org/2000/svg">
                                                    <use xlink:href="img/sprite_icons.svg#icon__cart" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                </svg>
                                            </i>
                                        </button>
                                    </div>
                                </div>
                            </div>

                            <div class="doc">
                                <div class="doc__image">
                                    <div class="doc__icon">
                                        <img src="img/icon__pdf.svg" class="img-fluid" alt="">
                                    </div>
                                </div>
                                <div class="doc__text">
                                    <h3>Arhitect-project, bERLIN Mauerstraße, 45</h3>
                                    <div class="doc__subtitle"><strong>Dokumentdatum:</strong> 11/03/2018</div>
                                    <p>
                                        Kameraden! Die Weiterentwicklung der verschiedenen Aktivitätsformen erfordert die Einführung und Modernisierung des Systems der Personalschulung und erfüllt dringende Bedürfnisse.
                                    </p>
                                </div>
                                <div class="doc__price"><span>€145</span></div>
                                <div class="doc__action">
                                    <div class="doc__action_wrap">
                                        <button class="btn_round btn_buy">
                                            <i>
                                                <svg class="ico-svg" viewBox="0 0 33 29" xmlns="http://www.w3.org/2000/svg">
                                                    <use xlink:href="img/sprite_icons.svg#icon__cart" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                </svg>
                                            </i>
                                        </button>
                                    </div>
                                </div>
                            </div>

                            <div class="doc">
                                <div class="doc__image">
                                    <div class="doc__icon">
                                        <img src="img/icon__word.svg" class="img-fluid" alt="">
                                    </div>
                                </div>
                                <div class="doc__text">
                                    <h3>Arhitect-project, bERLIN Mauerstraße, 45</h3>
                                    <div class="doc__subtitle"><strong>Dokumentdatum:</strong> 11/03/2018</div>
                                    <p>
                                        Kameraden! Die Weiterentwicklung der verschiedenen Aktivitätsformen erfordert die Einführung und Modernisierung des Systems der Personalschulung und erfüllt dringende Bedürfnisse.
                                    </p>
                                </div>
                                <div class="doc__price"><span>€145</span></div>
                                <div class="doc__action">
                                    <div class="doc__action_wrap">
                                        <a href="#" class="doc__action_icon">
                                            <img src="img/icon__trash.svg" class="img-fluid" alt="">
                                        </a>
                                        <a href="#" class="btn_text">im korb</a>
                                    </div>
                                </div>
                            </div>

                            <div class="project__bottom">
                                <p>Haben Sie Dokumente zu diesem Gebäude?</p>
                                <a href="#" class="btn_round btn_md">Senden Dokumente zum Verkauf</a>
                            </div>

                        </div>
                    </div>

                </div>
            </section>

            <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
            <!-- -->

        </div>


        <!-- Modal -->
        <?php include('inc/modal.inc.php') ?>
        <!-- -->

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->


    </body>
</html>
