<!doctype html>
<html class="no-js" lang="">

    <!-- Head -->
    <?php include('inc/head.inc.php') ?>
    <!-- -->

    <body>

        <div class="page page_bg">

            <!-- Header -->
            <?php include('inc/header.inc.php') ?>
            <!-- -->

            <section class="main">
                <div class="container">

                    <div class="heading">
                        <h1>Mein Korb (5)</h1>
                    </div>

                    <div class="content">
                        <div class="content__wrap">

                            <ul class="step">
                                <li class="active"><span>1. MEINE BESTELLUNGEN</span></li>
                                <li><span>2. ZAHLUNG</span></li>
                                <li><span>3. СКАЧАТЬ ФАЙЛЫ</span></li>
                            </ul>

                            <div class="cart_row">

                                <h3 class="color_blue">BERLIN, Mauerstraße, 45</h3>

                                <div class="cart">
                                    <a href="#" class="cart__image">
                                        <div class="cart__image_item">
                                            <img src="img/icon__word.svg" class="img-fluid" alt="">
                                        </div>
                                    </a>
                                    <div class="cart__content">
                                        <h3><a href="#">Arhitect-project, bERLIN Mauerstraße, 45</a></h3>
                                        <div class="cart__date"><strong>Dokumentdatum:</strong> 11/03/2018</div>
                                        <p>Kameraden! Die Weiterentwicklung der verschiedenen Aktivitätsformen erfordert die Einführung und Modernisierung des Systems der Personalschulung und erfüllt dringende Bedürfnisse. </p>
                                    </div>
                                    <div class="cart__price">
                                        <span>€145</span>
                                        <button type="button" class="cart__remove"></button>
                                    </div>

                                </div>

                                <div class="cart">
                                    <a href="#" class="cart__image">
                                        <div class="cart__image_item">
                                            <img src="img/icon__word.svg" class="img-fluid" alt="">
                                        </div>
                                    </a>
                                    <div class="cart__content">
                                        <h3><a href="#">Arhitect-project, bERLIN Mauerstraße, 45</a></h3>
                                        <div class="cart__date"><strong>Dokumentdatum:</strong> 11/03/2018</div>
                                        <p>Kameraden! Die Weiterentwicklung der verschiedenen Aktivitätsformen erfordert die Einführung und Modernisierung des Systems der Personalschulung und erfüllt dringende Bedürfnisse. </p>
                                    </div>
                                    <div class="cart__price">
                                        <span>€145</span>
                                        <button type="button" class="cart__remove"></button>
                                    </div>

                                </div>

                                <div class="cart">
                                    <a href="#" class="cart__image">
                                        <div class="cart__image_item">
                                            <img src="img/icon__word.svg" class="img-fluid" alt="">
                                        </div>
                                    </a>
                                    <div class="cart__content">
                                        <h3><a href="#">Arhitect-project, bERLIN Mauerstraße, 45</a></h3>
                                        <div class="cart__date"><strong>Dokumentdatum:</strong> 11/03/2018</div>
                                        <p>Kameraden! Die Weiterentwicklung der verschiedenen Aktivitätsformen erfordert die Einführung und Modernisierung des Systems der Personalschulung und erfüllt dringende Bedürfnisse. </p>
                                    </div>
                                    <div class="cart__price">
                                        <span>€145</span>
                                        <button type="button" class="cart__remove"></button>
                                    </div>

                                </div>

                                <h3 class="color_blue">BERLIN, Mauerstraße, 45</h3>

                                <div class="cart">
                                    <a href="#" class="cart__image">
                                        <div class="cart__image_item">
                                            <img src="img/icon__word.svg" class="img-fluid" alt="">
                                        </div>
                                    </a>
                                    <div class="cart__content">
                                        <h3><a href="#">Arhitect-project, bERLIN Mauerstraße, 45</a></h3>
                                        <div class="cart__date"><strong>Dokumentdatum:</strong> 11/03/2018</div>
                                        <p>Kameraden! Die Weiterentwicklung der verschiedenen Aktivitätsformen erfordert die Einführung und Modernisierung des Systems der Personalschulung und erfüllt dringende Bedürfnisse. </p>
                                    </div>
                                    <div class="cart__price">
                                        <span>€145</span>
                                        <button type="button" class="cart__remove"></button>
                                    </div>

                                </div>

                                <h3 class="color_blue">BERLIN, Mauerstraße, 45</h3>

                                <div class="cart">
                                    <a href="#" class="cart__image">
                                        <div class="cart__image_item">
                                            <img src="img/icon__word.svg" class="img-fluid" alt="">
                                        </div>
                                    </a>
                                    <div class="cart__content">
                                        <h3><a href="#">Arhitect-project, bERLIN Mauerstraße, 45</a></h3>
                                        <div class="cart__date"><strong>Dokumentdatum:</strong> 11/03/2018</div>
                                        <p>Kameraden! Die Weiterentwicklung der verschiedenen Aktivitätsformen erfordert die Einführung und Modernisierung des Systems der Personalschulung und erfüllt dringende Bedürfnisse. </p>
                                    </div>
                                    <div class="cart__price">
                                        <span>€145</span>
                                        <button type="button" class="cart__remove"></button>
                                    </div>

                                </div>

                            </div>

                            <div class="cart_auth">

                                <div class="row">
                                    <div class="col col-xs-12 col-md-6 col-xl-5 col-no-gutter">
                                        <form class="form">
                                            <div class="row">
                                                <div class="col col-xs-12 col-sm-4 col-no-gutter">
                                                </div>
                                                <div class="col col-xs-12 col-sm-8 col-no-gutter">
                                                    <h3>Login-Daten</h3>
                                                </div>
                                            </div>
                                            <div class="mb_15">
                                                <div class="row">
                                                    <div class="col col-xs-12 col-sm-4 col-no-gutter">
                                                        <div class="form_label form_label_flex"><span>Е-MAIL</span></div>
                                                    </div>
                                                    <div class="col col-xs-12 col-sm-8 col-no-gutter">
                                                        <input type="text" class="form_control form_control_md" name="" placeholder="">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="mb_15">
                                                <div class="row">
                                                    <div class="col col-xs-12 col-sm-4 col-no-gutter">
                                                        <div class="form_label form_label_flex"><span>PASSWORT</span></div>
                                                    </div>
                                                    <div class="col col-xs-12 col-sm-8 col-no-gutter">
                                                        <input type="text" class="form_control form_control_md" name="" placeholder="">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="mb_15">
                                                <div class="row">
                                                    <div class="col col-xs-12 col-sm-4 col-no-gutter">
                                                        <div class="form_label form_label_flex"><span>WIEDERHOLUNGS-PASSWORT</span></div>
                                                    </div>
                                                    <div class="col col-xs-12 col-sm-8 col-no-gutter">
                                                        <input type="text" class="form_control form_control_md" name="" placeholder="">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col col-xs-12 col-sm-4 col-no-gutter"></div>
                                                <div class="col col-xs-12 col-sm-8 col-no-gutter">
                                                    <button type="submit" class="btn_round btn_round_sm">REGISTRIERUNG</button>
                                                </div>
                                            </div>
                                        </form>

                                    </div>
                                    <div class="col col-xs-12 col-md-6 col-xl-5 col-xl-offset-2 col-no-gutter">
                                        <div class="cart_auth__login">
                                            <div class="cart_auth__login_label">Oder</div>
                                            <div class="cart_auth__login_button">
                                                <a href="#" class="btn_round btn_round_md">LOGIN AUF SITE</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="text-right">
                                <button type="submit" class="btn btn_lg">BEZAHLEN €645</button>
                            </div>

                        </div>
                    </div>

                </div>
            </section>

            <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
            <!-- -->

        </div>


        <!-- Modal -->
        <?php include('inc/modal.inc.php') ?>
        <!-- -->

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->


    </body>
</html>
