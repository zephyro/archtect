<!doctype html>
<html class="no-js" lang="">

    <!-- Head -->
    <?php include('inc/head.inc.php') ?>
    <!-- -->

    <body>

        <div class="page page_bg">

            <!-- Header -->
            <?php include('inc/header.inc.php') ?>
            <!-- -->

            <section class="main">
                <div class="container">

                    <div class="heading">
                        <h1>Mein Korb (5)</h1>
                    </div>

                    <div class="content">
                        <div class="content__wrap">

                            <ul class="step">
                                <li class="active"><span>1. MEINE BESTELLUNGEN</span></li>
                                <li><span>2. ZAHLUNG</span></li>
                                <li><span>3. СКАЧАТЬ ФАЙЛЫ</span></li>
                            </ul>

                            <div class="cart_row">

                                <h3>SELECT ZAHLUNGSWEISE</h3>

                                <div class="cart">
                                    <div class="cart__content center_box">
                                        <div class="cart__pay">
                                            <div class="cart__pay_name">VISA/MASTERCARD</div>
                                            <div class="cart__pay_icon">
                                                <img src="img/icon__card.png" class="img-fluid" alt="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="cart__price">
                                        <span>€145</span>
                                    </div>
                                    <div class="cart__action">
                                        <div class="cart__action_wrap">
                                            <button type="button" class="btn_round">bezahlen</button>
                                        </div>
                                    </div>

                                </div>

                                <div class="cart">
                                    <div class="cart__content center_box">
                                        <div class="cart__pay">
                                            <div class="cart__pay_name">PAYPAL</div>
                                            <div class="cart__pay_icon">
                                                <img src="img/icon__paypal.png" class="img-fluid" alt="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="cart__price">
                                        <span>€145</span>
                                    </div>
                                    <div class="cart__action">
                                        <div class="cart__action_wrap">
                                            <button type="button" class="btn_round">bezahlen</button>
                                        </div>
                                    </div>

                                </div>

                            </div>


                        </div>
                    </div>

                </div>
            </section>

            <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
            <!-- -->

        </div>


        <!-- Modal -->
        <?php include('inc/modal.inc.php') ?>
        <!-- -->

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->


    </body>
</html>
