<!doctype html>
<html class="no-js" lang="">

    <!-- Head -->
    <?php include('inc/head.inc.php') ?>
    <!-- -->

    <body>

        <div class="page page_bg">

            <!-- Header -->
            <?php include('inc/header.inc.php') ?>
            <!-- -->

            <section class="main">
                <div class="container">

                    <div class="heading">
                        <h1>Mein Korb (5)</h1>
                    </div>

                    <div class="content">
                        <div class="content__wrap">

                            <ul class="step">
                                <li class="active"><span>1. MEINE BESTELLUNGEN</span></li>
                                <li><span>2. ZAHLUNG</span></li>
                                <li><span>3. СКАЧАТЬ ФАЙЛЫ</span></li>
                            </ul>

                            <div class="cart_row">

                                <h3 class="color_blue">BERLIN, Mauerstraße, 45</h3>

                                <div class="cart">
                                    <a href="#" class="cart__image">
                                        <div class="cart__image_item">
                                            <img src="img/icon__word.svg" class="img-fluid" alt="">
                                        </div>
                                    </a>
                                    <div class="cart__content">
                                        <h3><a href="#">Arhitect-project, bERLIN Mauerstraße, 45</a></h3>
                                        <div class="cart__date"><strong>Dokumentdatum:</strong> 11/03/2018</div>
                                        <p>Kameraden! Die Weiterentwicklung der verschiedenen Aktivitätsformen erfordert die Einführung und Modernisierung des Systems der Personalschulung und erfüllt dringende Bedürfnisse. </p>
                                    </div>
                                    <div class="cart__action">
                                        <div class="cart__action_wrap">
                                            <button type="button" class="btn_round">bezahlen</button>
                                        </div>
                                    </div>

                                </div>

                                <div class="cart">
                                    <a href="#" class="cart__image">
                                        <div class="cart__image_item">
                                            <img src="img/icon__word.svg" class="img-fluid" alt="">
                                        </div>
                                    </a>
                                    <div class="cart__content">
                                        <h3><a href="#">Arhitect-project, bERLIN Mauerstraße, 45</a></h3>
                                        <div class="cart__date"><strong>Dokumentdatum:</strong> 11/03/2018</div>
                                        <p>Kameraden! Die Weiterentwicklung der verschiedenen Aktivitätsformen erfordert die Einführung und Modernisierung des Systems der Personalschulung und erfüllt dringende Bedürfnisse. </p>
                                    </div>
                                    <div class="cart__action">
                                        <div class="cart__action_wrap">
                                            <button type="button" class="btn_round">bezahlen</button>
                                        </div>
                                    </div>

                                </div>

                                <div class="cart">
                                    <a href="#" class="cart__image">
                                        <div class="cart__image_item">
                                            <img src="img/icon__word.svg" class="img-fluid" alt="">
                                        </div>
                                    </a>
                                    <div class="cart__content">
                                        <h3><a href="#">Arhitect-project, bERLIN Mauerstraße, 45</a></h3>
                                        <div class="cart__date"><strong>Dokumentdatum:</strong> 11/03/2018</div>
                                        <p>Kameraden! Die Weiterentwicklung der verschiedenen Aktivitätsformen erfordert die Einführung und Modernisierung des Systems der Personalschulung und erfüllt dringende Bedürfnisse. </p>
                                    </div>
                                    <div class="cart__action">
                                        <div class="cart__action_wrap">
                                            <button type="button" class="btn_round">bezahlen</button>
                                        </div>
                                    </div>

                                </div>

                                <h3 class="color_blue">BERLIN, Mauerstraße, 45</h3>

                                <div class="cart">
                                    <a href="#" class="cart__image">
                                        <div class="cart__image_item">
                                            <img src="img/icon__word.svg" class="img-fluid" alt="">
                                        </div>
                                    </a>
                                    <div class="cart__content">
                                        <h3><a href="#">Arhitect-project, bERLIN Mauerstraße, 45</a></h3>
                                        <div class="cart__date"><strong>Dokumentdatum:</strong> 11/03/2018</div>
                                        <p>Kameraden! Die Weiterentwicklung der verschiedenen Aktivitätsformen erfordert die Einführung und Modernisierung des Systems der Personalschulung und erfüllt dringende Bedürfnisse. </p>
                                    </div>
                                    <div class="cart__action">
                                        <div class="cart__action_wrap">
                                            <button type="button" class="btn_round">bezahlen</button>
                                        </div>
                                    </div>

                                </div>

                                <h3 class="color_blue">BERLIN, Mauerstraße, 45</h3>

                                <div class="cart">
                                    <a href="#" class="cart__image">
                                        <div class="cart__image_item">
                                            <img src="img/icon__word.svg" class="img-fluid" alt="">
                                        </div>
                                    </a>
                                    <div class="cart__content">
                                        <h3><a href="#">Arhitect-project, bERLIN Mauerstraße, 45</a></h3>
                                        <div class="cart__date"><strong>Dokumentdatum:</strong> 11/03/2018</div>
                                        <p>Kameraden! Die Weiterentwicklung der verschiedenen Aktivitätsformen erfordert die Einführung und Modernisierung des Systems der Personalschulung und erfüllt dringende Bedürfnisse. </p>
                                    </div>
                                    <div class="cart__action">
                                        <div class="cart__action_wrap">
                                            <button type="button" class="btn_round">bezahlen</button>
                                        </div>
                                    </div>

                                </div>

                            </div>

                        </div>
                    </div>

                </div>
            </section>

            <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
            <!-- -->

        </div>


        <!-- Modal -->
        <?php include('inc/modal.inc.php') ?>
        <!-- -->

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->


    </body>
</html>
