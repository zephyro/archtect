<!doctype html>
<html class="no-js" lang="">

    <!-- Head -->
    <?php include('inc/head.inc.php') ?>
    <!-- -->

    <body>

        <div class="page page_bg">

            <!-- Header -->
            <?php include('inc/header.inc.php') ?>
            <!-- -->

            <section class="main">
                <div class="container">

                    <div class="heading">
                        <h1>F.A.Q.</h1>
                    </div>

                    <div class="tags">
                        <a href="#">Liste der Fragen 1</a>
                        <a href="#">Liste der Fragen 2</a>
                        <a href="#">Liste der Fragen 3</a>
                    </div>

                    <div class="content">
                        <div class="content__wrap">

                            <h3>Liste der Fragen 1</h3>

                            <div class="blue_box">
                                <h4>Überweisen Sie Geld auf Ihr Konto</h4>
                                <p>Kameraden! Die Weiterentwicklung der verschiedenen Aktivitätsformen erfordert die Einführung und Modernisierung des Systems der Personalschulung und erfüllt dringende Bedürfnisse...Kameraden! Die Weiterentwicklung der verschiedenen Aktivitätsformen erfordert die Einführung und Modernisierung des Systems der Personalschulung und erfüllt dringende Bedürfnisse...</p>
                            </div>

                            <div class="blue_box">
                                <h4>Überweisen Sie Geld auf Ihr Konto</h4>
                                <p>Kameraden! Die Weiterentwicklung der verschiedenen Aktivitätsformen erfordert die Einführung und Modernisierung des Systems der Personalschulung und erfüllt dringende Bedürfnisse...Kameraden! Die Weiterentwicklung der verschiedenen Aktivitätsformen erfordert die Einführung und Modernisierung des Systems der Personalschulung und erfüllt dringende Bedürfnisse...</p>
                            </div>

                            <div class="blue_box">
                                <h4>Überweisen Sie Geld auf Ihr Konto</h4>
                                <p>Kameraden! Die Weiterentwicklung der verschiedenen Aktivitätsformen erfordert die Einführung und Modernisierung des Systems der Personalschulung und erfüllt dringende Bedürfnisse...Kameraden! Die Weiterentwicklung der verschiedenen Aktivitätsformen erfordert die Einführung und Modernisierung des Systems der Personalschulung und erfüllt dringende Bedürfnisse...</p>
                            </div>

                            <h3 class="pt_20">Liste der Fragen 2</h3>

                            <div class="blue_box">
                                <h4>Überweisen Sie Geld auf Ihr Konto</h4>
                                <p>Kameraden! Die Weiterentwicklung der verschiedenen Aktivitätsformen erfordert die Einführung und Modernisierung des Systems der Personalschulung und erfüllt dringende Bedürfnisse...Kameraden! Die Weiterentwicklung der verschiedenen Aktivitätsformen erfordert die Einführung und Modernisierung des Systems der Personalschulung und erfüllt dringende Bedürfnisse...</p>
                            </div>

                            <div class="blue_box">
                                <h4>Überweisen Sie Geld auf Ihr Konto</h4>
                                <p>Kameraden! Die Weiterentwicklung der verschiedenen Aktivitätsformen erfordert die Einführung und Modernisierung des Systems der Personalschulung und erfüllt dringende Bedürfnisse...Kameraden! Die Weiterentwicklung der verschiedenen Aktivitätsformen erfordert die Einführung und Modernisierung des Systems der Personalschulung und erfüllt dringende Bedürfnisse...</p>
                            </div>

                            <div class="blue_box">
                                <h4>Überweisen Sie Geld auf Ihr Konto</h4>
                                <p>Kameraden! Die Weiterentwicklung der verschiedenen Aktivitätsformen erfordert die Einführung und Modernisierung des Systems der Personalschulung und erfüllt dringende Bedürfnisse...Kameraden! Die Weiterentwicklung der verschiedenen Aktivitätsformen erfordert die Einführung und Modernisierung des Systems der Personalschulung und erfüllt dringende Bedürfnisse...</p>
                            </div>

                            <h3 class="pt_20">Liste der Fragen 2</h3>

                            <div class="blue_box">
                                <h4>Überweisen Sie Geld auf Ihr Konto</h4>
                                <p>Kameraden! Die Weiterentwicklung der verschiedenen Aktivitätsformen erfordert die Einführung und Modernisierung des Systems der Personalschulung und erfüllt dringende Bedürfnisse...Kameraden! Die Weiterentwicklung der verschiedenen Aktivitätsformen erfordert die Einführung und Modernisierung des Systems der Personalschulung und erfüllt dringende Bedürfnisse...</p>
                            </div>

                        </div>
                    </div>

                </div>
            </section>

            <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
            <!-- -->

        </div>


        <!-- Modal -->
        <?php include('inc/modal.inc.php') ?>
        <!-- -->

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->


    </body>
</html>
