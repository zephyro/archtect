<!doctype html>
<html class="no-js" lang="">

    <!-- Head -->
    <?php include('inc/head.inc.php') ?>
    <!-- -->

    <body>

        <div class="page page_bg">

            <!-- Header -->
            <?php include('inc/header.inc.php') ?>
            <!-- -->

            <section class="main">
                <div class="container">

                    <div class="heading">
                        <h1>Page</h1>
                    </div>

                    <div class="content">
                        <div class="content__wrap">

                            <h4><a href="#enter" class="btn_modal">Enter</a></h4>

                            <h4><a href="#reg" class="btn_modal">Registration</a></h4>

                            <h4><a href="#forgot" class="btn_modal">Forgot</a></h4>

                            <h4><a href="#forgot_error" class="btn_modal">Forgot Error</a></h4>

                            <h4><a href="#forgot_success" class="btn_modal">Forgot Success</a></h4>

                            <h4><a href="#forgot_thanks" class="btn_modal">Forgot Thanks</a></h4>

                        </div>
                    </div>

                </div>
            </section>

            <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
            <!-- -->

        </div>


        <!-- Modal -->
        <?php include('inc/modal.inc.php') ?>
        <!-- -->

        <!-- Enter -->
        <div class="hide">
            <div class="modal" id="enter">
                <div class="modal__wrap">
                    <div class="modal__top">
                        <a class="modal__back" href="#"></a>
                        <button class="modal__close" data-fancybox-close></button>
                    </div>

                    <div class="modal__heading">
                        <div class="modal__heading_title">Einloggen</div>
                        <div class="modal__heading_sub">oder</div>
                        <div class="modal__heading__text"><a href="#">registrieren</a></div>
                    </div>

                    <div class="modal__content">
                        <form class="form">
                            <div class="form_group form_row">
                                <div class="form_row__label">
                                    <label class="form_label form_label_flex"><span>Е-MAIL</span></label>
                                </div>
                                <div class="form_row__center">
                                    <input type="text" class="form_control form_control_md" name="" placeholder="">
                                </div>
                                <div class="form_row__label"></div>
                            </div>
                            <div class="form_group form_row">
                                <div class="form_row__label">
                                    <label class="form_label form_label_flex"><span>PASSWORT</span></label>
                                </div>
                                <div class="form_row__center">
                                    <input type="text" class="form_control form_control_md" name="" placeholder="">
                                </div>
                                <div class="form_row__label"></div>
                            </div>
                            <div class="form_row mb_30">
                                <div class="form_row__label"></div>
                                <div class="form_row__center">
                                    <div class="row">
                                        <div class="col col-xs-12 col-sm-6 col-no-gutter">
                                            <label class="form_checkbox">
                                                <input type="checkbox" name="" value="">
                                                <span>Erinnere dich an mich</span>
                                            </label>
                                        </div>
                                        <div class="col col-xs-12 col-sm-6 col-no-gutter text_right_sm">
                                            <a href="#">Passwort wiederherstellen</a>
                                        </div>
                                    </div>

                                </div>
                                <div class="form_row__label"></div>
                            </div>
                            <div class="form_row">
                                <div class="form_row__label"></div>
                                <div class="form_row__center">
                                    <button type="submit" class="btn_round btn_round_lg">Anmelden</button>
                                </div>
                                <div class="form_row__label"></div>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>
        <!-- -->

        <!-- Reg -->
        <div class="hide">
            <div class="modal" id="reg">
                <div class="modal__wrap">
                    <div class="modal__top">
                        <a class="modal__back" href="#"></a>
                        <button class="modal__close" data-fancybox-close></button>
                    </div>

                    <div class="modal__heading">
                        <div class="modal__heading_title">Registrierung</div>
                        <div class="modal__heading_sub">oder</div>
                        <div class="modal__heading__text"><a href="#">Eingang zum Gelände</a></div>
                    </div>

                    <div class="modal__content">
                        <form class="form">
                            <div class="form_group form_row">
                                <div class="form_row__label">
                                    <label class="form_label form_label_flex"><span>Е-MAIL</span></label>
                                </div>
                                <div class="form_row__center">
                                    <input type="text" class="form_control form_control_md" name="" placeholder="">
                                </div>
                                <div class="form_row__label"></div>
                            </div>
                            <div class="form_group form_row">
                                <div class="form_row__label">
                                    <label class="form_label form_label_flex"><span>PASSWORT</span></label>
                                </div>
                                <div class="form_row__center">
                                    <input type="text" class="form_control form_control_md" name="" placeholder="">
                                </div>
                                <div class="form_row__label"></div>
                            </div>
                            <div class="form_row mb_30">
                                <div class="form_row__label"></div>
                                <div class="form_row__center">
                                    <label class="form_checkbox">
                                        <input type="checkbox" name="" value="">
                                        <span>Ich stimme <a href="#">den Nutzungsbedingungen</a> der Website zu (a)</span>
                                    </label>

                                </div>
                                <div class="form_row__label"></div>
                            </div>
                            <div class="form_row">
                                <div class="form_row__label"></div>
                                <div class="form_row__center">
                                    <button type="submit" class="btn_round btn_round_lg">Registrierung</button>
                                </div>
                                <div class="form_row__label"></div>
                            </div>
                        </form>
                    </div>


                </div>
            </div>
        </div>
        <!-- -->

        <!-- Forgot -->
        <div class="hide">
            <div class="modal" id="forgot">
                <div class="modal__wrap">
                    <div class="modal__top">
                        <a class="modal__back" href="#"></a>
                        <button class="modal__close" data-fancybox-close></button>
                    </div>

                    <div class="modal__heading">
                        <div class="modal__heading_title pb_50">Passwort Wiederherstellung</div>
                    </div>

                    <div class="modal__content">
                        <form class="form">
                            <div class="form_group form_row">
                                <div class="form_row__label">
                                    <label class="form_label form_label_flex"><span>Е-MAIL</span></label>
                                </div>
                                <div class="form_row__center">
                                    <input type="text" class="form_control form_control_md" name="" placeholder="">
                                </div>
                                <div class="form_row__label"></div>
                            </div>
                            <div class="form_row">
                                <div class="form_row__label"></div>
                                <div class="form_row__center">
                                    <button type="submit" class="btn_round btn_round_lg">erholen</button>
                                </div>
                                <div class="form_row__label"></div>
                            </div>
                        </form>
                    </div>


                </div>
            </div>
        </div>
        <!-- -->

        <!-- Forgot Error -->
        <div class="hide">
            <div class="modal" id="forgot_error">
                <div class="modal__wrap">
                    <div class="modal__top">
                        <a class="modal__back" href="#"></a>
                        <button class="modal__close" data-fancybox-close></button>
                    </div>

                    <div class="modal__heading">
                        <div class="modal__heading_title pb_50">Passwort Wiederherstellung</div>
                    </div>

                    <div class="modal__content">
                        <form class="form">
                            <div class="form_group form_row">
                                <div class="form_row__label">
                                    <label class="form_label form_label_flex"><span>Е-MAIL</span></label>
                                </div>
                                <div class="form_row__center">
                                    <div class="form_input error">
                                        <input type="text" class="form_control form_control_md" name="" placeholder="">
                                        <span class="form_error">Diese E-Mail existiert nicht</span>
                                    </div>
                                </div>
                                <div class="form_row__label"></div>
                            </div>
                            <div class="form_row">
                                <div class="form_row__label"></div>
                                <div class="form_row__center">
                                    <button type="submit" class="btn_round btn_round_lg">wiederherstellen</button>
                                </div>
                                <div class="form_row__label"></div>
                            </div>
                        </form>
                    </div>


                </div>
            </div>
        </div>
        <!-- -->

        <!-- Forgot Success -->
        <div class="hide">
            <div class="modal" id="forgot_success">
                <div class="modal__wrap">
                    <div class="modal__top">
                        <a class="modal__back" href="#"></a>
                        <button class="modal__close" data-fancybox-close></button>
                    </div>

                    <div class="modal__heading">
                        <div class="modal__heading_title pb_50">Erfolgreich</div>
                    </div>

                    <div class="modal__content">
                        <div class="modal__content_text">
                            Ein Link zum Zurücksetzen des Passworts wurde an Sie gesendet.<br/>
                            Klicken Sie darauf, um ein neues Passwort festzulegen.
                        </div>
                        <div class="form_row">
                            <div class="form_row__label"></div>
                            <div class="form_row__center">
                                <button type="submit" class="btn_round btn_round_lg">Herunterfahren</button>
                            </div>
                            <div class="form_row__label"></div>
                        </div>
                    </div>


                </div>
            </div>
        </div>
        <!-- -->

        <!-- Forgot Recovery -->
        <div class="hide">
            <div class="modal" id="forgot_recovery">
                <div class="modal__wrap">

                    <div class="modal__top">
                        <a class="modal__back" href="#"></a>
                        <button class="modal__close" data-fancybox-close></button>
                    </div>

                    <div class="modal__heading">
                        <div class="modal__heading_title pb_50">Geben Sie Ihr neues Passwort ein</div>
                    </div>

                    <div class="modal__content">
                        <form class="form">
                            <div class="form_group form_row">
                                <div class="form_row__label">
                                    <label class="form_label form_label_flex"><span>PASSWORT</span></label>
                                </div>
                                <div class="form_row__center">
                                    <div class="form_input">
                                        <input type="text" class="form_control form_control_md" name="" placeholder="">
                                    </div>
                                </div>
                                <div class="form_row__label"></div>
                            </div>
                            <div class="form_row">
                                <div class="form_row__label"></div>
                                <div class="form_row__center">
                                    <button type="submit" class="btn_round btn_round_lg">wiederherstellen</button>
                                </div>
                                <div class="form_row__label"></div>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>
        <!-- -->

        <!-- thanks -->
        <div class="hide">
            <div class="modal" id="forgot_thanks">
                <div class="modal__wrap">

                    <div class="modal__top">
                        <a class="modal__back" href="#"></a>
                        <button class="modal__close" data-fancybox-close></button>
                    </div>

                    <div class="modal__heading">
                        <div class="modal__heading_title pb_50">ERFOLGREICH</div>
                    </div>

                    <div class="modal__lead">Ihr Passwort ist erfolgreich gerettet</div>

                    <div class="modal__content">
                        <div class="form_row">
                            <div class="form_row__label"></div>
                            <div class="form_row__center">
                                <button type="submit" class="btn_round btn_round_lg">SCHLIESSEN</button>
                            </div>
                            <div class="form_row__label"></div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- -->

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->


    </body>
</html>
